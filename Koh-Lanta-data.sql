delete from `contestants`;
delete from `seasons`;
delete from `participations`;
delete from sqlite_sequence
where name = "seasons"
    OR name = "contestants"
    OR name = "participations";
-- contestants
insert into contestants (
        `name`,
        `firstname`,
        `age`,
        `city`,
        `occupation`,
        `sex`
    )
VALUES (
        "Boyer",
        "Marie",
        22,
        "Saint-Marcellin",
        "Étudiante en communication",
        "F"
    ),
    (
        "Noël",
        "Guillaume",
        24,
        "Saint-Brieuc",
        "Loueur de voitures",
        "H"
    ),
    (
        "Florina",
        "Jean-Luc",
        50,
        "Toulon",
        "Officier de marine en retraite",
        "H"
    ),
    (
        "Gollen",
        "Gaël",
        18,
        "Brest",
        "Étudiante en gestion",
        "F"
    ),
    (
        "Levy",
        "Harry",
        44,
        "Marseille",
        "Prothésiste dentaire",
        "H"
    ),
    (
        "Acabado",
        "Sandra",
        19,
        "Athis-Mons",
        "Étudiante en langues étrangères",
        "F"
    ),
    (
        "Noël",
        "David",
        24,
        "Estérençuby",
        "Musicien",
        "H"
    ),
    (
        "Bertheau",
        "Stéphane",
        24,
        "Nanterre",
        "Diplômé en urbanisme",
        "H"
    ),
    (
        "Doyen",
        "Géraldine",
        27,
        "Avignon",
        "Agente d'assurance",
        "F"
    ),
    (
        "Valembois",
        "Michèle",
        58,
        "Toulon-sur-Allier",
        "Retraitée",
        "F"
    ),
    (
        "Lecomte",
        "William",
        32,
        "Tours",
        "Universitaire",
        "H"
    ),
    (
        "Avignon",
        "Françoise",
        41,
        "Arras",
        "Employée de mairie",
        "F"
    ),
    (
        "Bissol",
        "Romain",
        27,
        "Vigneux-sur-Seine",
        "Électricien",
        "H"
    ),
    (
        "Tanguy",
        "Patricia",
        31,
        "La Garenne-Colombes",
        "Secrétaire de direction",
        "F"
    ),
    (
        "Biras",
        "Guénaëlle",
        25,
        "Paris",
        "Graphiste publicitaire",
        "F"
    ),
    (
        "Nicolet",
        "Gilles",
        35,
        "Bordeaux",
        "Rédacteur pigiste",
        "H"
    ),
    (
        "Lemasson",
        "Xavier",
        29,
        "Mornant",
        "Kinésithérapeute",
        "H"
    ),
    (
        "Lamotte",
        "Caroline",
        28,
        "Strasbourg",
        "Assistante commerciale",
        "F"
    ),
    (
        "Fortier",
        "Céline",
        22,
        "Toulouse",
        "Hôtesse d'accueil",
        "F"
    ),
    (
        "Baradeau",
        "Nelly",
        24,
        "Bordeaux",
        "Barmaid",
        "F"
    ),
    (
        "Cambon",
        "Pierre",
        51,
        "Brignoles",
        "Gérant d'entreprise d'installations sportives",
        "H"
    ),
    (
        "Forteron",
        "Jacky",
        29,
        "Lille",
        "Maître d'hôtel",
        "H"
    ),
    (
        "Koupaki",
        "Adonis",
        33,
        "Noisy-le-Grand",
        "Directeur de centre de vacances",
        "H"
    ),
    (
        "Denis",
        "Jimmy",
        22,
        "Avignon",
        "Vendeur forain",
        "H"
    ),
    (
        "Penisson",
        "Béatrice Cloix",
        35,
        "Bandol",
        "Responsable commerciale en assurances",
        "F"
    ),
    (
        "Arguillère",
        "François-Xavier",
        24,
        "Rueil-Malmaison",
        "Joaillier créateur",
        "H"
    ),
    (
        "Vial",
        "Isabelle",
        36,
        "Nice",
        "Préparatrice en pharmacie",
        "F"
    ),
    (
        "Garnier",
        "Maud",
        25,
        "Nantes",
        "Mannequin",
        "F"
    ),
    (
        "Mondon",
        "Marianne",
        34,
        "Mauléon-Licharre",
        "Gendarme",
        "F"
    ),
    (
        "Lenoir",
        "Bernard",
        47,
        "Lyon",
        "Esthéticien",
        "H"
    ),
    (
        "Roy",
        "Nicolas",
        29,
        "Bressuire",
        "Intérimaire dans l'agro-alimentaire",
        "H"
    ),
    (
        "Fatnassi",
        "Amel",
        28,
        "Paris",
        "Assistante sociale",
        "F"
    ),
    (
        "Denikine",
        "Alexandra",
        27,
        "Paris",
        "Négociante en vins",
        "F"
    ),
    (
        "Cohen",
        "Candice",
        23,
        "Nogent-sur-Marne",
        "Étudiante en communication",
        "F"
    ),
    (
        "Jeandel",
        "Michel",
        41,
        "Bouzanville",
        "Artisan couvreur-zingueur",
        "H"
    ),
    (
        "Guilloux",
        "Sophie",
        40,
        "Nice",
        "Disc-jockey",
        "F"
    ),
    (
        "Bourdon",
        "Julie",
        22,
        "La Garenne-Colombes",
        "Serveuse en restauration",
        "F"
    ),
    (
        "Penard",
        "Linda",
        25,
        "Rennes",
        "Coiffeuse",
        "F"
    ),
    (
        "Huquet",
        "Philippe",
        43,
        "Romagnieu",
        "Ex chef d'entreprise",
        "H"
    ),
    (
        "Berard",
        "Alexandre",
        22,
        "Sannois",
        "Étudiant en école de commerce",
        "H"
    ),
    (
        "Lecourt",
        "Richard",
        32,
        "Manthelan",
        "Agriculteur",
        "H"
    ),
    (
        "Pommier",
        "Sylvie",
        26,
        "Bordeaux",
        "Secrétaire",
        "F"
    ),
    (
        "Loew",
        "Sébastien",
        29,
        "Garges-lès-Gonesse",
        "Libraire",
        "H"
    ),
    (
        "Dot",
        "Valérie",
        31,
        "Seine-et-Marne",
        "Hôtesse de l'air",
        "F"
    ),
    (
        "Patry",
        "Hélène",
        27,
        "Nice",
        "Professeure de lettres",
        "F"
    ),
    (
        "Zoughari",
        "Moundir",
        29,
        "Paris",
        "Moniteur de sport",
        "H"
    ),
    (
        "Sanchez",
        "Antoine",
        58,
        "Sault-de-Navailles",
        "Retraité de gendarmerie",
        "H"
    ),
    (
        "Niangane",
        "Moussa",
        22,
        "Bagneux",
        "Gérant de vidéo-club",
        "H"
    ),
    (
        "Bano",
        "Delphine",
        32,
        "Seynod",
        "Serveuse",
        "F"
    ),
    (
        "Seguin",
        "Isabelle",
        40,
        "Bourg-en-Bresse",
        "Brocanteuse",
        "F"
    ),
    (
        "Parente",
        "Nelson",
        36,
        "Grenoble",
        "Agents de joueurs de football",
        "H"
    ),
    (
        "Dittmar",
        "Vidéli",
        44,
        "Saint-Michel-de-Lanès",
        "Décorateur-sculpteur",
        "H"
    ),
    (
        "Bani",
        "Sabira",
        23,
        "Essonne",
        "Informaticienne",
        "F"
    ),
    (
        "Lemarié",
        "Vicky",
        26,
        "Haute-Savoie",
        "Auto-entrepreneur en électro-ménager",
        "F"
    ),
    (
        "Armand",
        "Jean-Bernard",
        54,
        "Haute-Garonne",
        "Cadre bancaire en retraite",
        "H"
    ),
    (
        "Lapicque",
        "Nathalie",
        28,
        "Remiremont",
        "Conseillère d'éducation",
        "F"
    ),
    (
        "Brauer",
        "Guillaume",
        20,
        "Var",
        "Étudiant en droit",
        "H"
    ),
    (
        "Héritier",
        "Odile",
        57,
        "Hauts-de-Seine",
        "Répétitrice",
        "F"
    ),
    (
        "Ehret-Mader",
        "Catherine",
        36,
        "Isère",
        "Gérante en boutique de prêt-à-porter",
        "F"
    ),
    (
        "Eeckhout",
        "Sophie",
        27,
        "Seine-Saint-Denis",
        "Sage-femme",
        "F"
    ),
    (
        "Oswald",
        "Alban",
        27,
        "Seine-et-Marne",
        "Étudiant en gestion",
        "H"
    ),
    (
        "Grégoire",
        "Amélie",
        25,
        "Isère",
        "Animatrice de centre aéré",
        "F"
    ),
    (
        "Lafite",
        "Romuald",
        26,
        "Caen",
        "Moniteur de fitness",
        "H"
    ),
    (
        "Andrès",
        "Raphaël",
        40,
        "Saône-et-Loire",
        "Soudeur",
        "H"
    ),
    (
        "Alario",
        "Linda",
        32,
        "Chalon-sur-Saône",
        "Gérante de boutique en prêt-à-porter",
        "F"
    ),
    (
        "Bordier",
        "Philippe",
        38,
        "Brassac",
        "Mécanicien",
        "H"
    ),
    (
        "Réant",
        "Aude",
        30,
        "Seine-et-Marne",
        "Maquilleuse",
        "F"
    ),
    (
        "Huguin",
        "Mathieu",
        21,
        "Meurthe-et-Moselle",
        "Étudiant en pharmacie",
        "H"
    ),
    (
        "Sébert",
        "Sylvie",
        43,
        "Seine-Saint-Denis",
        "Employée de pompes funèbres",
        "F"
    ),
    (
        "Colusso",
        "Caroline",
        29,
        "Pyrénées-Atlantiques",
        "Créatrice de mode",
        "F"
    ),
    (
        "Maréchal",
        "Marie-Cécile",
        21,
        "Yvelines",
        "Sans emploi",
        "F"
    ),
    (
        "Vigneron",
        "Éliane",
        40,
        "Var",
        "Quartier-maître",
        "F"
    ),
    ("Ramos", "Thierry", 38, "Drôme", "Boucher", "H"),
    (
        "Holaphong",
        "Sakhone",
        23,
        "Seine-et-Marne",
        "Commercial",
        "H"
    ),
    (
        "Villaseque",
        "Jérôme",
        33,
        "Haute-Savoie",
        "Gendarme",
        "H"
    ),
    (
        "Isoart",
        "Christine",
        35,
        "Var",
        "Gérante d'auto-école",
        "F"
    ),
    (
        "Baradji",
        "Coumba",
        22,
        "Hauts-de-Seine",
        "Serveuse",
        "F"
    ),
    (
        "Vaney",
        "Pierre",
        51,
        "Meurthe-et-Moselle",
        "Graphiste",
        "H"
    ),
    (
        "Lobjoie",
        "Véronique",
        37,
        "Aisne",
        "Cheffe de publicité",
        "F"
    ),
    ("Tournier", "Alexis", 28, "Rhône", "Voyant", "H"),
    (
        "Derradji",
        "Mohamed",
        24,
        "Hauts-de-Seine",
        "Chauffeur d'autocar",
        "H"
    ),
    (
        "Bordas",
        "Francis",
        59,
        "Dordogne",
        "Retraité",
        "H"
    ),
    (
        "Castel",
        "Clémence",
        20,
        "Haute-Garonne",
        "Étudiante en STAPS",
        "F"
    ),
    (
        "Amorsi",
        "Karine",
        24,
        "Haute-Savoie ",
        "Aide-soignante",
        "F"
    ),
    (
        "El",
        "Malika",
        33,
        "Savoie ",
        "Faled Gérante d'un centre de vacances",
        "F"
    ),
    (
        "Maire",
        "Nicolas-Benoit",
        42,
        "Gard ",
        "Moniteur de tennis",
        "H"
    ),
    (
        "Gonzague",
        "Nicolas",
        26,
        "Bouches-du-Rhône ",
        "Commercial dle sport",
        "H"
    ),
    (
        "Errin",
        "Jean-Claude",
        60,
        "Moselle ",
        "Magasinier",
        "H"
    ),
    (
        "Tailleur",
        "Mickaël",
        32,
        "Yvelines ",
        "Ébéniste",
        "H"
    ),
    (
        "Granger",
        "Estelle",
        33,
        "Landes ",
        "Boulangère",
        "F"
    ),
    (
        "Bella",
        "Nathalie",
        38,
        "Vienne ",
        "Cheffe d'entreprise",
        "F"
    ),
    ("Ponton", "Alain", 49, "Marne ", "Meunier", "H"),
    (
        "Brun",
        "Catherine",
        43,
        "Bouches-du-Rhône ",
        "Mère au foyer",
        "F"
    ),
    (
        "Cordonnier",
        "Gaëlle",
        27,
        "Haute-Savoie ",
        "Contremaître",
        "F"
    ),
    (
        "Bayard",
        "Marie",
        21,
        "Jura ",
        "Esthéticienne",
        "F"
    ),
    (
        "Laresche",
        "Ludovic",
        28,
        "Doubs ",
        "Maître fromager",
        "H"
    ),
    (
        "Roullé",
        "Sébastien",
        30,
        "Seine-Maritime ",
        "Chef d'entreprise",
        "H"
    ),
    (
        "Frahi",
        "Émilie",
        24,
        "Isère ",
        "Conseillère en assurances",
        "F"
    ),
    (
        "Cardonnel",
        "François-David",
        21,
        "Gard",
        "Escrimeur",
        "H"
    ),
    (
        "Jaffar",
        "Ali",
        35,
        "Seine-Saint-Denis",
        "Conducteur de métro",
        "H"
    ),
    (
        "Bodin",
        "Pascale",
        39,
        "Charente-Maritime",
        "Cheffe d'entreprise",
        "F"
    ),
    (
        "Dagniaux",
        "Marie-Laure",
        23,
        "Paris",
        "Vendeuse de prêt-à-porter",
        "F"
    ),
    (
        "Guyon",
        "Mélanie",
        24,
        "Doubs",
        "Coiffeuse",
        "F"
    ),
    (
        "Lambert",
        "Véronique",
        50,
        "Nord",
        "Conseillère financière",
        "F"
    ),
    (
        "Vavasseur",
        "Chloé",
        25,
        "Seine-Maritime",
        "Chargée de ressources humaines",
        "F"
    ),
    (
        "Africaa",
        "Érick",
        32,
        "Gard",
        "Coach sportif",
        "H"
    ),
    (
        "Fortes",
        "Filomène",
        35,
        "Hauts-de-Seine",
        "Gestionnaire en assurances",
        "F"
    ),
    (
        "Torrin",
        "Adrien",
        55,
        "Savoie",
        "Agent de la SNCF retraité",
        "H"
    ),
    (
        "Delachaux",
        "Grégoire",
        24,
        "Yvelines",
        "Jardinier",
        "H"
    ),
    (
        "Boudes",
        "Laurent",
        34,
        "Hérault",
        "Grossiste en coquillages",
        "H"
    ),
    (
        "Brasier De Thuy",
        "Patrick",
        40,
        "Nord",
        "Chef d'entreprise",
        "H"
    ),
    (
        "Quintilla",
        "Simon",
        26,
        "Aude",
        "Bûcheron",
        "H"
    ),
    (
        "Hemelsdael",
        "Maryline",
        36,
        "Nord",
        "Agricultrice",
        "F"
    ),
    (
        "Handi",
        "Souad",
        24,
        "Haute-Garonne",
        "Gérante d'un centre de loisirs",
        "F"
    ),
    (
        "Cuoco",
        "Kévin",
        21,
        "Var",
        "Pilote de karting",
        "H"
    ),
    (
        "Tartacède-Bollaert",
        "Valérie",
        41,
        "Paris",
        "Experte-géomètre",
        "F"
    ),
    (
        "Coulibaly",
        "Céga",
        23,
        "Seine-Saint-Denis",
        "Magasinier",
        "H"
    ),
    (
        "Cissé",
        "Irya",
        23,
        "Val-de-Marne",
        "Mannequin",
        "F"
    ),
    (
        "Rodriguès",
        "Morgane",
        33,
        "Finistère",
        "Coiffeuse",
        "F"
    ),
    (
        "Joullié",
        "Jessica",
        20,
        "Hérault",
        "Étudiante en communication",
        "F"
    ),
    ("Garcia", "Alain", 38, "Isère", "Boulanger", "H"),
    (
        "Hauton-Arnaud",
        "Jean-Bernard",
        40,
        "Bouches-du-Rhône",
        "Agent d'assurance",
        "H"
    ),
    (
        "Lavaud",
        "Christopher",
        25,
        "Val-de-Marne",
        "Gérant de société de compléments alimentaires",
        "H"
    ),
    (
        "Guttatoro-Martin",
        "Christophe",
        39,
        "Nord",
        "Gérant de pizzeria",
        "H"
    ),
    (
        "Hoffness",
        "Charlène",
        20,
        "Bas-Rhin",
        "Étudiante en lettres",
        "F"
    ),
    (
        "Djellali",
        "Abdelhakim",
        36,
        "Loire-Atlantique",
        "Éboueur",
        "H"
    ),
    (
        "Colombier",
        "Régis",
        56,
        "Hérault",
        "Commerçant",
        "H"
    ),
    (
        "Rapin",
        "Carole",
        40,
        "Loire-Atlantique",
        "Créatrice de bijoux",
        "F"
    ),
    (
        "Ensargueix",
        "Nathalie",
        29,
        "Seine-Saint-Denis",
        "Professeure de mathématiques",
        "F"
    ),
    (
        "Bolle",
        "Bertrand",
        32,
        "Pyrénées-Orientales",
        "Pilote d'hélicoptère",
        "H"
    ),
    (
        "Favier",
        "Frédéric",
        31,
        "Isère",
        "Charpentier",
        "H"
    ),
    (
        "Gauzet",
        "Christelle",
        28,
        "Gironde",
        "Fonctionnaire de police",
        "F"
    ),
    (
        "Bourlier",
        "Joëlle",
        62,
        "Pyrénées-Orientales",
        "Professeure de sport à la retraite",
        "F"
    ),
    (
        "Desvergne",
        "Myriam",
        56,
        "Seine-Saint-Denis",
        "Formatrice financière",
        "F"
    ),
    (
        "Mairesse",
        "Julien",
        25,
        "Maine-et-Loire",
        "Diplômé en tourisme",
        "H"
    ),
    (
        "Parra",
        "Anthony",
        24,
        "Bouches-du-Rhône",
        "Ambulancier",
        "H"
    ),
    (
        "Rolet",
        "Claire",
        26,
        "Doubs",
        "Responsable d'un magasin de décoration",
        "F"
    ),
    (
        "Mocellin",
        "Marine",
        19,
        "Haute-Savoie",
        "Peintre en bâtiment",
        "F"
    ),
    (
        "Ferdjani",
        "Kaouther",
        26,
        "Paris",
        "Attachée consulaire",
        "F"
    ),
    (
        "Amico",
        "Alexandre",
        31,
        "Nord",
        "Fonctionnaire de police",
        "H"
    ),
    (
        "Boucher",
        "Freddy",
        23,
        "Nord",
        "Élève ingénieur en informatique",
        "H"
    ),
    (
        "Nickert",
        "Pascal",
        39,
        "Bas-Rhin",
        "Cadre SNCF",
        "H"
    ),
    (
        "Léger",
        "Louis-Laurent",
        28,
        "Essonne",
        "Directeur d'agence d'import-export",
        "H"
    ),
    (
        "Navarro",
        "Raphaële",
        29,
        "Hérault",
        "Responsable d'achats informatiques",
        "F"
    ),
    (
        "Assi",
        "Rodolphe",
        52,
        "Bouches-du-Rhône",
        "Carrossier",
        "H"
    ),
    (
        "Rebaï",
        "Kader",
        45,
        "Maine-et-Loire",
        "Responsable sécurité",
        "H"
    ),
    (
        "Lefebvre-Trehoux",
        "Fabienne",
        44,
        "Alpes-Maritimes",
        "Assistante maternelle",
        "F"
    ),
    (
        "Da Silva",
        "Isabelle",
        35,
        "Yvelines",
        "Commerçante en prêt-à-porter",
        "F"
    ),
    (
        "Merle",
        "Patrick",
        38,
        "Loire",
        "Ouvrier teinturier",
        "H"
    ),
    (
        "Chevry",
        "Christina",
        22,
        "Bouches-du-Rhône",
        "Gestionnaire de stock",
        "F"
    ),
    (
        "Lise",
        "Betty",
        37,
        "Fort-de-France",
        "Gérante d'un centre de remise en forme (+ Record de France de triple saut (1997-2007))",
        "F"
    ),
    (
        "Lebœuf",
        "Frank",
        42,
        "Marseille",
        "Comédien (+ Champion du monde de football (1998))",
        "H"
    ),
    (
        "Peizerat",
        "Gwendal",
        37,
        "Lyon",
        "Chef d'entreprise d'équipements sportifs (+ Champion olympique de danse sur glace (2002))",
        "H"
    ),
    (
        "Bouras",
        "Djamel",
        38,
        "Givors",
        "Consultant sportif (+ Champion olympique de judo (1996))",
        "H"
    ),
    (
        "Jossinet",
        "Frédérique",
        34,
        "Rosny-sous-Bois",
        "Judokate (+ Vice-championne olympique de judo (2004))",
        "F"
    ),
    (
        "Khris",
        "Taïg",
        34,
        "Algérie",
        "Présentateur d'une émission sur les sports extrêmes (+ Champion du monde de roller en ligne (2000-2001))",
        "H"
    ),
    (
        "Lamare",
        "Myriam",
        35,
        "Saint-Denis",
        "Boxeuse (+ Championne du monde de boxe (2004-2006))",
        "F"
    ),
    (
        "Brugiroux",
        "Frédérique",
        45,
        "Gard",
        "Toiletteuse pour animaux",
        "F"
    ),
    (
        "Escouflaire",
        "Vivien",
        20,
        "Val-d'Oise",
        "Lycéen",
        "H"
    ),
    (
        "Merdel",
        "Alain",
        47,
        "Gard",
        "Confiturier",
        "H"
    ),
    (
        "Mercadal",
        "Virginie",
        38,
        "Var",
        "Pioli Coiffeuse",
        "F"
    ),
    (
        "Lieutaud",
        "Jennifer",
        23,
        "Bouches-du-Rhône",
        "Commerciale",
        "F"
    ),
    (
        "Pigato",
        "Jean-Pierre",
        47,
        "Bouches-du-Rhône",
        "Cadre territorial",
        "H"
    ),
    (
        "Perez",
        "Audrey",
        24,
        "Loire",
        "En recherche d'emploi",
        "F"
    ),
    (
        "Deltell",
        "Boris",
        30,
        "Paris",
        "Responsable marketing",
        "H"
    ),
    (
        "D'Hoore",
        "Valentin",
        20,
        "Nord",
        "Étudiant",
        "H"
    ),
    (
        "Akriche",
        "Abdellah",
        32,
        "Hauts-de-Seine",
        "Éducateur sportif",
        "H"
    ),
    (
        "Plissonneau",
        "Marine",
        35,
        "Val-de-Marne",
        "Hôtesse de l'air",
        "F"
    ),
    (
        "Gasser",
        "Aurélie",
        26,
        "Meurthe-et-Moselle",
        "Animatrice dans un centre de loisirs",
        "F"
    ),
    (
        "Pizzocchia",
        "Laurence",
        33,
        "ans Drapeau de la Suisse Suisse",
        "Styliste ongulaire",
        "F"
    ),
    (
        "M'Babacéré",
        "Kunlé",
        28,
        "Essonne",
        "Pakorona Animateur social",
        "H"
    ),
    (
        "Prouteau",
        "Véronique",
        55,
        "Tarn-et-Garonne",
        "Bègue Retraitée",
        "F"
    ),
    (
        "El Mejjad",
        "Wafa",
        26,
        "Haute-Garonne",
        "Mejjad Vendeuse en prêt-à-porter",
        "F"
    ),
    (
        "Dartois",
        "Claude",
        30,
        "Paris",
        "Chauffeur de maître",
        "H"
    ),
    (
        "Duron",
        "Philippe",
        41,
        "Drôme",
        "Rempailleur",
        "H"
    ),
    (
        "Legouverneur",
        "Lisa",
        25,
        "Moselle",
        "Cadre commerciale",
        "F"
    ),
    (
        "Diard",
        "Catherine",
        53,
        "Sarthe",
        "Cheffe comptable",
        "F"
    ),
    (
        "Lemercier",
        "Steve",
        24,
        "Marne",
        "Adjoint de sécurité dans la police nationale",
        "H"
    ),
    (
        "Icardi",
        "Gérard",
        65,
        "Eure",
        "Gérant d'un parc animalier",
        "H"
    ),
    (
        "Hadjaf",
        "Nawal",
        25,
        "Seine-Saint-Denis",
        "Footballeuse freestyle",
        "F"
    ),
    (
        "Mahieux",
        "Benoît",
        31,
        "Oise",
        "Responsable d'études statistiques",
        "H"
    ),
    (
        "Fettache",
        "Délisia",
        21,
        "Nord",
        "Animatrice d'un centre social",
        "F"
    ),
    (
        "Matteucci",
        "Caroline",
        40,
        "ans Suisse",
        "Coache en développement personnel",
        "F"
    ),
    (
        "Boulet",
        "Maxime",
        20,
        "ans Belgique",
        "Étudiant en sciences politiques",
        "H"
    ),
    (
        "Amar",
        "Anthony",
        23,
        "Meurthe-et-Moselle",
        "Professeur de fitness",
        "H"
    ),
    (
        "Moenaert",
        "Olivier",
        43,
        "Nord",
        "Cadre commercial",
        "H"
    ),
    (
        "Delbarre Reuter",
        "Florence",
        39,
        "Hérault",
        "Éducatrice",
        "F"
    ),
    (
        "Jarry",
        "Virginie",
        32,
        "Ardennes",
        "Intérimaire",
        "F"
    ),
    (
        "Morel",
        "Patricia",
        36,
        "Lot",
        "Chauffeure-routière",
        "F"
    ),
    (
        "Pouillon",
        "Alexandra",
        29,
        "Paris",
        "Hôtesse d’accueil dans un musée",
        "F"
    ),
    (
        "Bazin",
        "Martin",
        23,
        "Hauts-de-Seine",
        "Chef d'entreprise",
        "H"
    ),
    (
        "Gbezan",
        "Ella",
        25,
        "ans Belgique",
        "Étudiante en psychologie",
        "F"
    ),
    (
        "Maistret",
        "Laurent",
        29,
        "Val-de-Marne",
        "Mannequin",
        "H"
    ),
    (
        "Teahui",
        "Teheiura",
        33,
        "Ain",
        "Cuisinier",
        "H"
    ),
    (
        "Urdampilleta",
        "Gérard",
        41,
        "Pyrénées-Atlantiques",
        "Jardinier",
        "H"
    ),
    (
        "Boimare",
        "Mélanie",
        30,
        "Hauts-de-Seine",
        "Boulangère",
        "F"
    ),
    (
        "Kalfallah",
        "Mickaël",
        57,
        "Isère",
        "Chauffeur de taxi",
        "H"
    ),
    (
        "Tallon",
        "Sara",
        42,
        "Var",
        "Gérante d'un centre de remise en forme",
        "F"
    ),
    (
        "De",
        "Nadine",
        57,
        "Haute-Savoie",
        "Carpentry Coordinatrice de transplantation d'organe",
        "F"
    ),
    (
        "Sadeler",
        "Élodie",
        36,
        "Charente",
        "Mère au foyer",
        "F"
    ),
    (
        "Parmentier",
        "Marie",
        23,
        "Belgique",
        "Gérante d'un restaurant",
        "F"
    ),
    (
        "Dito",
        "Anthony",
        24,
        "Bouches-du-Rhône",
        "Plombier",
        "H"
    ),
    (
        "Rodriguez",
        "Javier",
        39,
        "Belgique",
        "Peintre en bâtiment",
        "H"
    ),
    (
        "Thaï Thaï",
        "Namadia",
        28,
        "Val-de-Marne",
        "Chauffeur-livreur",
        "H"
    ),
    (
        "Sidibé",
        "Marylou",
        22,
        "Seine-Saint-Denis",
        "Hôtesse de l'air",
        "F"
    ),
    (
        "Perez",
        "Catherine",
        41,
        "Yonne",
        "Commerciale dans le bâtiment",
        "F"
    ),
    (
        "Sold",
        "Camille",
        18,
        "Bas-Rhin",
        "Étudiante en STAPS",
        "F"
    ),
    (
        "Clément",
        "Charles",
        20,
        "Yvelines",
        "Étudiant en architecture",
        "H"
    ),
    (
        "Villette",
        "Thierry",
        44,
        "Alpes-Maritimes",
        "Magnétiseur",
        "H"
    ),
    (
        "Modde",
        "Myriam",
        34,
        "Aisne",
        "Préparatrice en pharmacie",
        "F"
    ),
    (
        "Deniaud",
        "Bernard",
        60,
        "Loire-Atlantique",
        "Retraité",
        "H"
    ),
    (
        "Bizet",
        "Philippe",
        41,
        "Seine-Maritime",
        "Ouvrier en traitement de déchets",
        "H"
    ),
    (
        "Alvarez",
        "Vanessa",
        37,
        "Gironde",
        "Ostréicultrice",
        "F"
    ),
    (
        "Martinet",
        "Brice",
        30,
        "Loiret",
        "Mannequin",
        "H"
    ),
    (
        "Lartiche",
        "Ugo",
        30,
        "Pyrénées-Orientales",
        "Fauconnier",
        "H"
    );
--
-- participations
--
insert into participations (season_id, contestant_id)
values (1, 1),
    (1, 2),
    (1, 3),
    (1, 4),
    (1, 5),
    (1, 6),
    (1, 7),
    (1, 8),
    (1, 9),
    (1, 10),
    (1, 11),
    (1, 12),
    (1, 13),
    (1, 14),
    (1, 15),
    (1, 16),
    (2, 17),
    (2, 18),
    (2, 19),
    (2, 20),
    (2, 21),
    (2, 22),
    (2, 23),
    (2, 24),
    (2, 25),
    (2, 26),
    (2, 27),
    (2, 28),
    (2, 29),
    (2, 30),
    (2, 31),
    (2, 32),
    (3, 33),
    (3, 34),
    (3, 35),
    (3, 36),
    (3, 37),
    (3, 38),
    (3, 39),
    (3, 40),
    (3, 41),
    (3, 42),
    (3, 43),
    (3, 44),
    (3, 45),
    (3, 46),
    (3, 47),
    (3, 48),
    (3, 49),
    (3, 50),
    (4, 51),
    (4, 52),
    (4, 53),
    (4, 54),
    (4, 55),
    (4, 56),
    (4, 57),
    (4, 58),
    (4, 59),
    (4, 60),
    (4, 61),
    (4, 62),
    (4, 63),
    (4, 64),
    (4, 65),
    (4, 66),
    (5, 67),
    (5, 68),
    (5, 69),
    (5, 70),
    (5, 71),
    (5, 72),
    (5, 73),
    (5, 74),
    (5, 75),
    (5, 76),
    (5, 77),
    (5, 78),
    (5, 79),
    (5, 80),
    (5, 81),
    (5, 82),
    (5, 83),
    (6, 84),
    (6, 85),
    (6, 86),
    (6, 87),
    (6, 88),
    (6, 89),
    (6, 90),
    (6, 91),
    (6, 92),
    (6, 93),
    (6, 94),
    (6, 95),
    (6, 96),
    (6, 97),
    (6, 98),
    (6, 99),
    (7, 100),
    (7, 101),
    (7, 102),
    (7, 103),
    (7, 104),
    (7, 105),
    (7, 106),
    (7, 107),
    (7, 108),
    (7, 109),
    (7, 110),
    (7, 111),
    (7, 112),
    (7, 113),
    (7, 114),
    (7, 115),
    (8, 116),
    (8, 117),
    (8, 118),
    (8, 119),
    (8, 120),
    (8, 121),
    (8, 122),
    (8, 123),
    (8, 124),
    (8, 125),
    (8, 126),
    (8, 127),
    (8, 128),
    (8, 129),
    (8, 130),
    (8, 131),
    (8, 132),
    (9, 32),
    (9, 3),
    (9, 46),
    (9, 132),
    (9, 66),
    (9, 83),
    (9, 98),
    (9, 99),
    (9, 47),
    (9, 59),
    (9, 122),
    (9, 107),
    (9, 114),
    (9, 63),
    (10, 133),
    (10, 134),
    (10, 135),
    (10, 136),
    (10, 137),
    (10, 138),
    (10, 139),
    (10, 140),
    (10, 141),
    (10, 142),
    (10, 143),
    (10, 144),
    (10, 145),
    (10, 146),
    (10, 147),
    (10, 148),
    (10, 149),
    (10, 150),
    (11, 151),
    (11, 152),
    (11, 153),
    (11, 154),
    (11, 155),
    (11, 156),
    (11, 157),
    (11, 81),
    (11, 150),
    (11, 65),
    (11, 77),
    (11, 124),
    (11, 141),
    (11, 109),
    (12, 158),
    (12, 159),
    (12, 160),
    (12, 161),
    (12, 162),
    (12, 163),
    (12, 164),
    (12, 165),
    (12, 166),
    (12, 167),
    (12, 168),
    (12, 169),
    (12, 170),
    (12, 171),
    (12, 172),
    (12, 173),
    (12, 174),
    (12, 175),
    (13, 176),
    (13, 177),
    (13, 178),
    (13, 179),
    (13, 180),
    (13, 181),
    (13, 182),
    (13, 183),
    (13, 184),
    (13, 185),
    (13, 186),
    (13, 187),
    (13, 188),
    (13, 189),
    (13, 190),
    (13, 191),
    (13, 192),
    (13, 193),
    (13, 194),
    (13, 195),
    (14, 147),
    (14, 168),
    (14, 31),
    (14, 148),
    (14, 189),
    (14, 82),
    (14, 141),
    (14, 173),
    (14, 194),
    (14, 48),
    (14, 15),
    (14, 149),
    (14, 77),
    (14, 28),
    (14, 174),
    (14, 130),
    (15, 196),
    (15, 197),
    (15, 198),
    (15, 199),
    (15, 200),
    (15, 201),
    (15, 202),
    (15, 203),
    (15, 204),
    (15, 205),
    (15, 206),
    (15, 207),
    (15, 208),
    (15, 209),
    (15, 210),
    (15, 211),
    (15, 212),
    (15, 213),
    (15, 214),
    (15, 215);
--
-- seasons
--
insert into seasons (`year`, `name`)
values (2001, "Les aventuriers de Koh-Lanta"),
    (2002, "Nicoya"),
    (2003, "Bocas del Toro"),
    (2004, "Panama"),
    (2005, "Pacifique"),
    (2006, "Vanuatu"),
    (2007, "Palawan"),
    (2008, "Caramoan"),
    (2009, "Le Retour des héros"),
    (2009, "Palau"),
    (2010, "Le Choc des héros"),
    (2010, "Viêtnam"),
    (2011, "Raja Ampat"),
    (2012, "La Revanche des Héros"),
    (2012, "Malaisie"),
    (2013, "Koh Rong"),
    (2014, "La Nouvelle Édition"),
    (2015, "Johor"),
    (2016, "Thaïlande"),
    (2016, "L'Île au Trésor"),
    (2017, "Cambodge"),
    (2017, "Fidji"),
    (2018, "Le Combat des Héros"),
    (2018, "Kadavu"),
    (2019, "La Guerre des Chefs"),
    (2020, "L'Île des héros"),
    (2020, "Les 4 Terres"),
    (2021, "La Légende"),
    (2021, "Les Armes Secrètes"),
    (2022, "Le Totem Maudit");
UPDATE seasons
SET special = 1
where name in (
        "Le Retour des héros",
        "Le Choc des héros",
        "La Nouvelle Édition",
        "Le Combat des Héros",
        "L'Île des héros",
        "La Revanche des Héros",
        "La Légende"
    );
UPDATE seasons
SET canceled = 1
where name in ("Kadavu", "Koh Rong");
--
-- Queries
--
-- Seasons
select ROW_NUMBER() OVER(
        order by id
    ) as Seasons,
    id,
    year,
    name
from seasons
where special = 0;
select ROW_NUMBER() OVER(
        order by id
    ) as Seasons,
    id,
    year,
    name
from seasons
where special = 1;
-- All contestants adventures
select c.id,
    c.name,
    c.firstname,
    s.name,
    s.year
from contestants c
    join participations p on p.contestant_id = c.id
    join seasons s on p.season_id = s.id
order by s.year DESC;
-- Number of participations for min = 2
select c.firstname,
    c.name,
    COUNT(c.id) as 'number of participation'
from contestants c
    join participations p on p.contestant_id = c.id
group by (c.id)
having count(c.id) > 1;
-- Nombre de participants par saisons
select count(*) as 'Nombre de participants',
    s.name
from contestants c
    join participations p on c.id = p.contestant_id
    join seasons s on s.id = p.season_id
group by s.id;
-- Duplicate contestants
select name || ' ' || firstname,
    count(*)
from contestants
group by name || ' ' || firstname
HAVING count(*) > 1;
-- Sex parity
select *
from contestants;
select sex,
    COUNT(*)
from contestants
group by sex;