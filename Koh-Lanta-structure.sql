DROP TABLE IF EXISTS contestants;
DROP TABLE IF EXISTS participations;
DROP TABLE IF EXISTS tribes;
DROP TABLE IF EXISTS challenges;
DROP TABLE IF EXISTS eliminations;
DROP TABLE IF EXISTS votes;
DROP TABLE IF EXISTS episodes;
DROP TABLE IF EXISTS seasons;
CREATE TABLE contestants (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name VARCHAR2(100),
    firstname VARCHAR2(100),
    age NUMBER,
    city VARCHAR2(100),
    occupation VARCHAR2(100),
    sex VARCHAR2(1)
);
CREATE TABLE participations (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    season_id INTEGER,
    contestant_id INTEGER,
    FOREIGN KEY (season_id) REFERENCES seasons(id),
    FOREIGN KEY (contestant_id) REFERENCES contestants(id)
);
CREATE TABLE tribes (id int PRIMARY KEY);
CREATE TABLE challenges (id int PRIMARY KEY);
CREATE TABLE eliminations (id int PRIMARY KEY);
CREATE TABLE votes (`id` int PRIMARY KEY);
CREATE TABLE episodes (`id` int PRIMARY KEY);
CREATE TABLE seasons (
    `id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `year` NUMBER,
    `name` VARCHAR2(100),
    `special` BIT DEFAULT 0,
    `canceled` BIT DEFAULT 0
);